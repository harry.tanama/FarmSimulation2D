/*
    Copyright (C) 2024
    by Harry Tanama

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY.

    See the COPYING file for more details.
*/

#ifndef USERINTERFACE_H
#define USERINTERFACE_H

#include "Game.h"
#include "TextureManager.h"

class UserInterface
{
public:
    UserInterface(glm::vec2 position, glm::vec2 scale, const char* filename)
    {
        mTexture = TextureManager::LoadTexture(filename);
        SDL_QueryTexture(mTexture, 0, 0, &textureWidth, &textureHeight);

        this->mScale = scale; 
        this->mPosition = position;

        srcRect.x = 0;
        srcRect.y = 0;
        srcRect.w = textureWidth;
        srcRect.h = textureHeight;

        dstRect.x = mPosition.x;
        dstRect.y = mPosition.y;
        dstRect.w = textureWidth * mScale.x;
        dstRect.h = textureHeight * mScale.y;
    }

    ~UserInterface()
    {
        SDL_DestroyTexture(mTexture);
    }
    
    void Update()
    {}
    
    void Render()
    {
        TextureManager::Draw(mTexture, srcRect, dstRect, SDL_FLIP_NONE);  
    }
    
    void putItemOnTheUI(const char* filename, const int index)
    {
        mTexture = TextureManager::LoadTexture(filename);
        SDL_QueryTexture(mTexture, 0, 0, &textureWidth, &textureHeight);

        mItemStore[index];

        srcRect.x = 0;
        srcRect.y = 0;
        srcRect.w = textureWidth;
        srcRect.h = textureHeight;

        dstRect.x = static_cast<int>(mPosition.x);
        dstRect.y = static_cast<int>(mPosition.y);
        dstRect.w = static_cast<int>(textureWidth* mScale.x);
        dstRect.h = textureHeight* mScale.y;
    }

    int getItem(const int index)
    {
        return mItemStore[index];
    }

private:
    glm::vec2 mPosition;
    glm::vec2 mScale = glm::vec2(1.0, 1.0);
    int textureWidth;
    int textureHeight; 
    int mItemStore[5];

    SDL_Texture *mTexture;
    SDL_Rect srcRect, dstRect;

};
#endif