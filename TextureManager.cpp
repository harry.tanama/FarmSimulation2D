/*
	Copyright (C) 2024
	by Harry Tanama

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY.

	See the COPYING file for more details.
*/

#include "TextureManager.h"

SDL_Texture *TextureManager::LoadTexture(const char *filename)
{
    SDL_Surface* tempSurface = IMG_Load(filename);
	SDL_Texture* tempTexture = SDL_CreateTextureFromSurface(Game::GameRenderer, tempSurface);
	SDL_FreeSurface(tempSurface);
    
    return tempTexture;
    //after returning the texture to the called function, all the pointers are destoryed and cleared out of scope.
}

void TextureManager::Draw(SDL_Texture* texture, SDL_Rect src, SDL_Rect dest, SDL_RendererFlip flip)
{
    SDL_RenderCopyEx(Game::GameRenderer, texture, &src, &dest, NULL, NULL, flip);
}
