/*
	Copyright (C) 2024
	by Harry Tanama

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY.

	See the COPYING file for more details.
*/

#include "Game.h"
#include "Entity.h"
#include "SDL_render.h"
#include "Tile.h"
#include "UserInterface.h"
#include "InsideTheHouse.h"
#include <iostream>
#include <SDL_ttf.h>

// static SDL_Renderer *GameRenderer;
SDL_Renderer* Game::GameRenderer = NULL; 
SDL_Event Game::event;
bool Game::RenderGameState;
bool Game::areYouInsideTheHouse;
bool isPrintingDialogeFinish = false;
int i = 0;

UserInterface *farmUI;
Entity *player;
Entity *house;
Entity *carrotSeed;
Entity *bed;
Entity *table;

Tile *door;
Tile *door2;

Tile *soil1;
Tile *soil2;
Tile *soil3;
Tile *soil4;
Tile *soil5;
Tile *soil6;
Tile *tree;
Tile *fence1;
Tile *fence2;
Tile *fence3;
Tile *dialogueBox; // temporary before creating real object
Tile *characterPotrait; // temporary before creating real object                   

TTF_Font *font = NULL;
SDL_Surface *textSurface = NULL; 

Game::Game() { mGameIsRunning = false; }

Game::~Game() 
{
	SDL_DestroyRenderer(GameRenderer);
	SDL_DestroyWindow(mGameWindow);
}

void Game::init()
{
    if( SDL_Init (SDL_INIT_EVERYTHING) != 0)
	{
	    printf( "SDL cannot initialize: %s\n", IMG_GetError() );
		return;
	}
    
	SDL_DisplayMode displayMode;
	SDL_GetCurrentDisplayMode(0, &displayMode);
	// windowWidth = displayMode.w;
    ///windowHeight = displayMode.h;

	mGameWindow = SDL_CreateWindow("Game Menu", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WINDOW_SHOWN);

    if (!mGameWindow)
	{
		printf( "SDL_Window could not initialize! mGameWindow Error: %s\n", SDL_GetError() );
		return;
	}

    GameRenderer = SDL_CreateRenderer(mGameWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!GameRenderer)
	{
		printf( "SDL_Renderer could not initialize! GameRenderer Error: %s\n", SDL_GetError() );
		return;
	}

	BGTexture = TextureManager::LoadTexture("assets/outdoorBG.png"); 
	outdoorBackGround = {0,0,1024,800};

    if (TTF_Init() < 0){
        printf("FAILED to initialized TTF_Init: %s\n", TTF_GetError() );
        return;
    }

//	SDL_SetWindowFullscreen(mGameWindow, SDL_WINDOW_FULLSCREEN);
    mGameIsRunning = true;	
	Beginning();
}

void Game::Beginning()
{
	SDL_SetRenderDrawColor(GameRenderer, 55, 125, 34, 255);
	door = new Tile(glm::vec2(69.0, 125.0), glm::vec2(2.0, 2.0),"assets/closedoor.png");
	tree = new Tile (glm::vec2(windowWidth-900, windowHeight-300), glm::vec2 (2.0,2.0), "assets/appleTree.png");
	house = new Entity(glm::vec2(0.0, 0.0), glm::vec2(2.0, 2.0),"assets/house.png");
	farmUI = new UserInterface(glm::vec2((windowWidth/2) - 150, windowHeight-70), glm::vec2(2.0, 2.0),"assets/FarmerUI.png");
			
	carrotSeed = new Entity(glm::vec2(100.0, 80.0), glm::vec2 (2.0, 2.0), "assets/carrotSeed.png");
	
	player = new Entity(glm::vec2(73.0, 185), glm::vec2(1.0, 1.0),"assets/player.png");

	fence1 = new Tile (glm::vec2(340.0, 0.0), glm::vec2 (3.0, 3.0), "assets/leftcornerfence.png");
	fence2 = new Tile (glm::vec2(380.0, 0.0), glm::vec2 (3.0, 3.0), "assets/middlefence.png");
	fence3 = new Tile (glm::vec2(420.0, 0.0), glm::vec2 (3.0, 3.0), "assets/rightcornerfence.png");

	soil1 = new Tile (glm::vec2(350.0, 50.0), glm::vec2 (3.0, 3.0), "assets/tilthsoil.png");
	soil2 = new Tile (glm::vec2(410.0, 50.0), glm::vec2 (3.0, 3.0), "assets/tilthsoil.png");
	soil3 = new Tile (glm::vec2(470.0, 50.0), glm::vec2 (3.0, 3.0), "assets/tilthsoil.png");
	soil4 = new Tile (glm::vec2(350.0, 110.0), glm::vec2 (3.0, 3.0), "assets/tilthsoil.png");
	soil5 = new Tile (glm::vec2(410.0, 110.0), glm::vec2 (3.0, 3.0), "assets/tilthsoil.png");
	soil6 = new Tile (glm::vec2(470.0, 110.0), glm::vec2 (3.0, 3.0), "assets/tilthsoil.png");
	
    dialogueBox = new Tile(glm::vec2(260.0, 480.0), glm::vec2 (1.0, 1.0), "./assets/dialogue/dialogueBox2.png");
    characterPotrait = new Tile(glm::vec2(0.0, 0.0), glm::vec2 (3.0, 3.0), "./assets/character-potrait.png");

    font = TTF_OpenFont("./assets/FreeMono.ttf", 20);
    if(font == NULL)
    {
        printf("\033[1;31mFAILED to load font! SDL_ttf Error: %s\n\033[0m", TTF_GetError() );
        return;
    }

	RenderGameState = true;


}

void Game::ProcessInput() {

    while (SDL_PollEvent(&event)) {
        
		CollisionCheck();
		SDL_GetMouseState(&mousePosition.x, &mousePosition.y);

		if (Game::event.type == SDL_KEYDOWN)
		{
			switch (Game::event.key.keysym.sym)
			{
			case SDLK_w:
                player->MoveUp();
				break;
			case SDLK_a:
				player->MoveLeft();
				break;
			case SDLK_d:
				player->MoveRight();				
				break;
			case SDLK_s:
				player->MoveDown();				
				break;
			case SDLK_ESCAPE:
                mGameIsRunning = false;
                break;
			}

			printf("Position X: %f, Position Y: %f \n", player->getPositionX(), player->getPositionY());	
		}

		if(Game::event.type == SDL_MOUSEBUTTONDOWN)
		{
			if(event.button.button == SDL_BUTTON_RIGHT)
			{
				printf("mouse position x=%i y=%i",mousePosition.x, mousePosition.y);
			}
			break;
		}
		
		if (Game::event.type == SDL_KEYUP)
		{
			switch (Game::event.key.keysym.sym)
			{
			case SDLK_w:
				break;
			case SDLK_a:
				break;
			case SDLK_d:
				break;
			case SDLK_s:
				break;
			}
		}
    
		switch (Game::event.type) {
            case SDL_QUIT:
                mGameIsRunning = false;
                break;            
        }
	}
}

void Game::Run()
{
	while(mGameIsRunning)
	{
		ProcessInput();
		Update();

		if(RenderGameState)
		{
			Render();
		}

		if(areYouInsideTheHouse)
		{
			RenderInsideTheHouse();
		}

	}
}

void Game::CollisionCheck()
{
    if (
		player->getPositionX() < soil1->getRectPositionX() + (soil1->getRectWidth() - mCollisionOffSet) &&
		player->getPositionX() + (player->getRectWidth() - mCollisionOffSet) > soil1->getRectPositionX() &&
		player->getPositionY() < soil1->getRectPositionY() + (soil1->getRectHeight() - mCollisionOffSet) &&
		player->getPositionY() + (player->getRectHeight() - mCollisionOffSet) > soil1->getRectPositionY()
	)
	{
	
		if(Game::event.type == SDL_MOUSEMOTION){}
			if(Game::event.type == SDL_MOUSEBUTTONDOWN){
				soil1->setTexture("assets/tilthsoilWseed.png");
			}
	}

	if (
		player->getPositionX() < soil2->getRectPositionX() + (soil2->getRectWidth()-10) &&
		player->getPositionX() + (player->getRectWidth() - mCollisionOffSet) > soil2->getRectPositionX() &&
		player->getPositionY() < soil2->getRectPositionY() + (soil2->getRectHeight() - mCollisionOffSet) &&
		player->getPositionY() + (player->getRectHeight() - mCollisionOffSet) > soil2->getRectPositionY()
	)
	{
		if(Game::event.type == SDL_MOUSEMOTION){}
			if(Game::event.type == SDL_MOUSEBUTTONDOWN){
				soil2->setTexture("assets/tilthsoilWseed.png");
			}
	}

	if (
		player->getPositionX() < soil3->getRectPositionX() + (soil3->getRectWidth() - mCollisionOffSet) &&
		player->getPositionX() + (player->getRectWidth() - mCollisionOffSet) > soil3->getRectPositionX() &&
		player->getPositionY() < soil3->getRectPositionY() + (soil3->getRectHeight() - mCollisionOffSet) &&
		player->getPositionY() + ( player->getRectHeight() - mCollisionOffSet) > soil3->getRectPositionY()
	)
	{
		if(Game::event.type == SDL_MOUSEMOTION){}
			if(Game::event.type == SDL_MOUSEBUTTONDOWN){
				soil3->setTexture("assets/tilthsoilWseed.png");
			}
	}

	if (
		player->getPositionX() < soil4->getRectPositionX() + soil4->getRectWidth()- mCollisionOffSet &&
		player->getPositionX() + player->getRectWidth() - mCollisionOffSet > soil4->getRectPositionX() &&
		player->getPositionY() < soil4->getRectPositionY() + soil4->getRectHeight() - mCollisionOffSet &&
		player->getPositionY() + player->getRectHeight() - mCollisionOffSet > soil4->getRectPositionY()
	)
	{
		if(Game::event.type == SDL_MOUSEMOTION){}
			if(Game::event.type == SDL_MOUSEBUTTONDOWN){
				soil4->setTexture("assets/tilthsoilWseed.png");
			}
	}

	if (
		player->getPositionX() < soil5->getRectPositionX() + soil5->getRectWidth() - mCollisionOffSet &&
		player->getPositionX() + player->getRectWidth() - mCollisionOffSet > soil5->getRectPositionX() &&
    	player->getPositionY() < soil5->getRectPositionY() + soil5->getRectHeight() - mCollisionOffSet &&
		player->getPositionY() + player->getRectHeight() - mCollisionOffSet > soil5->getRectPositionY()
	)
	{
		if(Game::event.type == SDL_MOUSEMOTION){}
			if(Game::event.type == SDL_MOUSEBUTTONDOWN){
				soil5->setTexture("assets/tilthsoilWseed.png");
			}
	}

	if (
		player->getPositionX() < soil6->getRectPositionX() + soil6->getRectWidth() - mCollisionOffSet &&
		player->getPositionX() + player->getRectWidth() - mCollisionOffSet > soil6->getRectPositionX() &&
		player->getPositionY() < soil6->getRectPositionY() + soil6->getRectHeight() - mCollisionOffSet &&
		player->getPositionY() + player->getRectHeight() - mCollisionOffSet > soil6->getRectPositionY()
	)
	{
		if(Game::event.type == SDL_MOUSEMOTION){}
			if(Game::event.type == SDL_MOUSEBUTTONDOWN){
				soil6->setTexture("assets/tilthsoilWseed.png");
			}
	}

    if (
        player->getPositionX() < door->getRectPositionX() + door->getRectWidth() &&
        player->getPositionX() + player->getRectWidth() > door->getRectPositionX() &&
        player->getPositionY() < door->getRectPositionY() + door->getRectHeight() &&
        player->getPositionY() + player->getRectHeight() > door->getRectPositionY())
    {	
		if(Game::event.type == SDL_MOUSEMOTION){}
			if(Game::event.type == SDL_MOUSEBUTTONDOWN){
				printf("try opening door\n");
				RenderGameState = false;
				areYouInsideTheHouse = true;
				SetupInsideTheHouse();
			}
	}
}

void Game::Update()
{
	player->Update();
}

/*/ Function to render text to a texture
SDL_Texture* Game::renderTextToTexture(SDL_Renderer* renderer, TTF_Font* font, const std::string& text, SDL_Color textColor) 
{
    SDL_Surface* surface = TTF_RenderText_Solid(font, text.c_str(), textColor);
    if (!surface) {
        std::cerr << "Failed to render text surface: " << TTF_GetError() << std::endl;
        // Handle error (text rendering failed)
        return nullptr;
    }

    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
    if (!texture) {
        std::cerr << "Failed to create texture from surface: " << SDL_GetError() << std::endl;
        // Handle error (texture creation failed)
    }

    SDL_FreeSurface(surface); // Free the surface, as it is no longer needed
    return texture;
}
*/

void Game::Render()
{	
    SDL_Texture *textureForText;
    int textWidth = 0;
    int textHeight = 0; 
    Uint32 wrapLength = 400;
    SDL_Color textColor = { 0, 0, 0, 255 };
    
    std::string text = "I try to print this dialogue letter by letter  until all the sentences are completly printed on the screen. And then I just printed all the sentences to the screen and keep looping the completed sentences to the screen as you see here!";   
    std::string tmpStr;
   
    if (!isPrintingDialogeFinish)
    {
        while( i < text.length() )
        {        
            textSurface = TTF_RenderText_Solid_Wrapped(font, tmpStr.c_str(), textColor, wrapLength);
            tmpStr += text[i];
        
            textureForText = SDL_CreateTextureFromSurface(GameRenderer, textSurface);
            SDL_FreeSurface(textSurface); // free the surface to avoid memory overflow with multiple surfaces in game loop.
        
            SDL_QueryTexture(textureForText, NULL, NULL, &textWidth, &textHeight);
            SDL_Rect destRect = { 300, 520, textWidth, textHeight };
        
            SDL_RenderClear(GameRenderer);

            SDL_RenderCopy(GameRenderer, BGTexture, NULL, &outdoorBackGround);

            house->Render();
            soil1->Render();
            soil2->Render();
            soil3->Render();
            soil4->Render();
            soil5->Render();
            soil6->Render();
            fence1->Render();
            fence2->Render();
            fence3->Render();
            tree->Render();
            carrotSeed->Render();
            door->Render();
            player->Render();
            farmUI->Render();

            dialogueBox->Render();
            characterPotrait->Render();      
                
            SDL_RenderCopy(GameRenderer, textureForText, NULL, &destRect);

            SDL_RenderPresent(GameRenderer);
            SDL_Delay(100);
            // release resources to void memory overflow creating multiple textures in the game loop
            SDL_DestroyTexture(textureForText);
            
            if (i >= text.length()-1)
            {
                isPrintingDialogeFinish = true;
                break;
            }
         
            i++;   
        }
    }
    else
    {
        SDL_RenderClear(GameRenderer);
        
        textSurface = TTF_RenderText_Solid_Wrapped(font, text.c_str(), textColor, wrapLength);
        textureForText = SDL_CreateTextureFromSurface(GameRenderer, textSurface);
        SDL_FreeSurface(textSurface);

        SDL_QueryTexture(textureForText, NULL, NULL, &textWidth, &textHeight);
        SDL_Rect destRect = { .x=300, .y=520, .w=textWidth, .h=textHeight };         
        
        SDL_RenderCopy(GameRenderer, BGTexture, NULL, &outdoorBackGround);
        
        house->Render();
        soil1->Render();
        soil2->Render();
        soil3->Render();
        soil4->Render();
        soil5->Render();
        soil6->Render();
        fence1->Render();
        fence2->Render();
        fence3->Render();
        tree->Render();
        carrotSeed->Render();
        door->Render();
        player->Render();
        farmUI->Render();

        dialogueBox->Render();
        characterPotrait->Render();  

        SDL_RenderCopy( GameRenderer, textureForText, NULL, &destRect);
    
        SDL_RenderPresent(GameRenderer);

        // release resources to void memory overflow creating multiple textures in the game loop
        SDL_DestroyTexture(textureForText);   
    }
}

void Game::CleanUP()
{
	SDL_DestroyRenderer(GameRenderer);
	SDL_DestroyWindow(mGameWindow);
   	SDL_Quit();
}

// Player Inside The House 
void Game::SetupInsideTheHouse()
{	
	player = new Entity(glm::vec2(512.00, 620.00), glm::vec2(1.0, 1.0),"assets/player.png");
	bed = new Entity(glm::vec2(300.00, 300.00), glm::vec2(3.0, 3.0),"assets/bed.png");
	table = new Entity(glm::vec2(172.00, 300.00), glm::vec2(3.0, 3.0),"assets/table.png");
	farmUI = new UserInterface(glm::vec2((windowWidth/2) - 150, windowHeight-70), glm::vec2(2.0, 2.0),"assets/FarmerUI.png");
    door2 = new Tile(glm::vec2(windowWidth/2, windowHeight-100), glm::vec2(2.0, 2.0), "assets/closedoor.png");
}



void Game::CollisionCheckInsideTheHouse()
{
	if (
		player->getPositionX() < door2->getRectPositionX() + door2->getRectWidth() &&
		player->getPositionX() + player->getRectWidth() > door2->getRectPositionX() &&
		player->getPositionY() < door2->getRectPositionY() + door2->getRectHeight() &&
		player->getPositionY() + player->getRectHeight() > door2->getRectPositionY()
	)
	{	
		if(Game::event.type == SDL_MOUSEMOTION){}
			if(Game::event.type == SDL_MOUSEBUTTONDOWN){
				printf("try opening door\n");
				RenderGameState = true;
				areYouInsideTheHouse = false;
				SetupOutFromTheHouse(); // setup player outside the house.
			}

	}
}
void Game::SetupOutFromTheHouse()
{
	player = new Entity(glm::vec2(75.0, 220.0), glm::vec2(1.0, 1.0),"assets/player.png");
    i = 0; 
    isPrintingDialogeFinish = false; 
}

void Game::RenderInsideTheHouse()
{
	SDL_SetRenderDrawColor(GameRenderer, 240, 155, 89, 255);	
	SDL_RenderClear(GameRenderer);

	CollisionCheckInsideTheHouse();

	door2->Render();
	bed->Render();
	table->Render();
	player->Render();

	SDL_RenderPresent(GameRenderer);
}
