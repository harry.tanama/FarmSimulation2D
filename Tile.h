/*
    Copyright (C) 2024
    by Harry Tanama

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY.

    See the COPYING file for more details.
*/

#ifndef TILE_H
#define TILE_H

#include "Game.h"

class Tile
{
private:
    int ID;
    char type; 

    SDL_Texture *mTexture;
    SDL_Rect srcRect, dstRect;
    glm::vec2 mPosition;
    glm::vec2 mScale = glm::vec2(1.0, 1.0);
    int textureWidth;
    int textureHeight; 

public:
    Tile(glm::vec2 position, glm::vec2 scale, const char* filename);
    ~Tile();
    void Update();
    void Render();   
    
    int GetID() const;
    int GetNumberOfTie();
    void setTexture(const char* filename);

    int getRectPositionX() const;
    int getRectPositionY() const;
    int getRectWidth() const;
    int getRectHeight() const; 

    static int numberOfTile; 
};
#endif