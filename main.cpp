/*
	Copyright (C) 2024
	by Harry Tanama

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY.

	See the COPYING file for more details.
*/

#include "Game.h"

int main (int argc, char *argv[])
{
    Game game;

    game.init();
    game.Run();
    game.CleanUP();
        
    return 0; 
}