/*
    Copyright (C) 2024
    by Harry Tanama

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY.

    See the COPYING file for more details.
*/

#ifndef INSIDETHEHOUSE_H
#define INSIDETHEHOUSE_H

#include "Game.h"
#include "Entity.h"

class InsideTheHouse
{
public:

    InsideTheHouse(Entity *player)
    {
        player = new Entity(glm::vec2(512.00, 620.00), glm::vec2(1.0, 1.0),"assets/player.png");

    }

    void Update()
    {

    }

    void Render()
    {

    }

};
#endif