/*
    Copyright (C) 2024
    by Harry Tanama

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY.

    See the COPYING file for more details.
*/

#ifndef ENTITY_H
#define ENTITY_H

#include "Game.h"    
#include "TextureManager.h"

class Entity
{
    private:
    glm::vec2 mPosition;
    glm::vec2 mScale = glm::vec2(1.0, 1.0);
    glm::vec2 mSpeed = glm::vec2(2.0, 2.0);

    SDL_Texture *mTexture;
    SDL_Rect srcRect, dstRect;
    SDL_RendererFlip spriteFlip = SDL_FLIP_NONE;
    int textureWidth;
    int textureHeight; 
    
    public:
        ~Entity(){
            SDL_DestroyTexture(mTexture);
        }

        Entity(glm::vec2 position, glm::vec2 scale, const char* filename)
        {
            mTexture = TextureManager::LoadTexture(filename);
            SDL_QueryTexture(mTexture, 0, 0, &textureWidth, &textureHeight);

            this->mScale = scale;
            mPosition = position;

            srcRect.x = 0;
            srcRect.y = 0;
            srcRect.w = textureWidth;
            srcRect.h = textureHeight;

            dstRect.x = static_cast<int>(mPosition.x);
            dstRect.y = static_cast<int>(mPosition.y);
            dstRect.w = static_cast<int>(textureWidth* mScale.x);
            dstRect.h = textureHeight* mScale.y;
        }

        void Update()
        {
            dstRect.x = static_cast<int>(mPosition.x);
            dstRect.y = static_cast<int>(mPosition.y);
            dstRect.w = static_cast<int>(textureWidth* mScale.x);
            dstRect.h = static_cast<int>(textureHeight* mScale.y);
        }
        
        void Render()
        {
            TextureManager::Draw(mTexture, srcRect, dstRect, spriteFlip);            
        }

        void setTexture(const char* filename)
        {
            mTexture = TextureManager::LoadTexture(filename);
            SDL_QueryTexture(mTexture, 0, 0, &textureWidth, &textureHeight);
            srcRect.x = 0;
            srcRect.y = 0;
            srcRect.w = textureWidth;
            srcRect.h = textureHeight;
        }
        
        void setTexture(glm::vec2 position, glm::vec2 scale, const char* filename)
        {
            mTexture = TextureManager::LoadTexture(filename);
            SDL_QueryTexture(mTexture, 0, 0, &textureWidth, &textureHeight);

            this->mScale = scale;
            this->mPosition = position;

            srcRect.x = 0;
            srcRect.y = 0;
            srcRect.w = textureWidth;
            srcRect.h = textureHeight;

            dstRect.x = static_cast<int>(mPosition.x);
            dstRect.y = static_cast<int>(mPosition.y);
            dstRect.w = static_cast<int>(textureWidth* mScale.x);
            dstRect.h = textureHeight* mScale.y;
        }

        void MoveRight()
        {
            mPosition.x += 3;
        }

        void MoveLeft()
        {
            mPosition.x -= 3;
        }

        void MoveUp()
        {
            mPosition.y -= 3; 
        }

        void MoveDown()
        {
            mPosition.y += 3; 
        }

        float getPositionX()
        {
            return mPosition.x;
        }

        float getPositionY()
        {
            return mPosition.y;
        }

        int getRectPositionX() const
        {
            return dstRect.x;
        }

        int getRectPositionY() const
        {
            return dstRect.y;
        }

        int getRectWidth() const
        {
            return dstRect.w;
        }

        int getRectHeight() const
        {
            return dstRect.h;
        }
};

#endif
