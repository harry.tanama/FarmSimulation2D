/*
    Copyright (C) 2024
    by Harry Tanama

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY.

    See the COPYING file for more details.
*/

#include "Tile.h"
#include "TextureManager.h"

int Tile::numberOfTile = 0;

Tile::Tile(glm::vec2 position, glm::vec2 scale, const char* filename)
{
    mTexture = TextureManager::LoadTexture(filename);
    SDL_QueryTexture(mTexture, 0, 0, &textureWidth, &textureHeight);

    this->mScale = scale; 
    this->mPosition = position;
    this->ID = ++numberOfTile;

    srcRect.x = 0;
    srcRect.y = 0;
    srcRect.w = textureWidth;
    srcRect.h = textureHeight;

    dstRect.x = mPosition.x;
    dstRect.y = mPosition.y;
    dstRect.w = textureWidth * mScale.x;
    dstRect.h = textureHeight * mScale.y;
}

Tile::~Tile()
{
    SDL_DestroyTexture(mTexture);
}
void Tile::setTexture(const char* filename)
{
    mTexture = TextureManager::LoadTexture(filename);
    SDL_QueryTexture(mTexture, 0, 0, &textureWidth, &textureHeight);
    srcRect.x = 0;
    srcRect.y = 0;
    srcRect.w = textureWidth;
    srcRect.h = textureHeight;

}

void Tile::Update()
{
}

void Tile::Render()
{
    TextureManager::Draw(mTexture, srcRect, dstRect, SDL_FLIP_NONE);  
}

int Tile::GetID() const
{
    return ID;
}

int Tile::GetNumberOfTie()
{
    return numberOfTile;
}

int Tile::getRectPositionX() const
{
    return dstRect.x;
}

int Tile::getRectPositionY() const
{
    return dstRect.y;
}

int Tile::getRectWidth() const
{
    return dstRect.w;
}

int Tile::getRectHeight() const
{
    return dstRect.h;
}
