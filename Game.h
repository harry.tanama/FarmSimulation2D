/*
    Copyright (C) 2024
    by Harry Tanama

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY.

    See the COPYING file for more details.
*/

#ifndef GAME_H
#define GAME_H

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include "glm/glm.hpp"

class Game
{
public: 
    Game();
    ~Game();
    void init();
    void Beginning();
    void Run();
    void ProcessInput();
    void Update();
    void Render();
    void CleanUP();
    void CollisionCheck();
    
    void SetupInsideTheHouse();
    void SetupOutFromTheHouse();
    void RenderInsideTheHouse();
    void CollisionCheckInsideTheHouse();

    static SDL_Renderer *GameRenderer;
    static SDL_Event event;
    static bool RenderGameState;
    static bool areYouInsideTheHouse;

private:
    SDL_Window *mGameWindow;
    SDL_Texture *BGTexture;

    const int windowWidth = 1024;
    const int windowHeight = 800;

    bool mGameIsRunning;
    int mCollisionOffSet = 40;

    SDL_Point mousePosition;
    SDL_Rect outdoorBackGround;    
    
};
#endif