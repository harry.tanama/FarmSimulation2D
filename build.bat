@echo off
::call "C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvars32.bat"

set SDL_FILES=C:\SDL2\include
set SDL_LIBS=C:\SDL2\lib\x64

cl -Zi /Fe:test_FarmLiving.exe main.cpp Game.cpp TextureManager.cpp Tile.cpp /I %SDL_FILES% /link /LIBPATH:%SDL_LIBS%^
 /SUBSYSTEM:WINDOWS shell32.lib SDL2.lib SDL2main.lib SDL2_image.lib
::start testMSVC32.exe
